# Practica001

# Este es un ejemplo para codear scripts en php e ir tomando confianza en cuanto al desarrollo de estructuras de archivos relacionales
# Se Pretende que el alumno genere estructura de archivos relaciones, lea el requerimiento, genere las consultas necesarias luego de comprender el requerimiento
# Exponga una idea de solución y luego de la misma realice el desarrollo de la solución

### En el proyecto recibiran los siguientes archivos:
* code39.php libreria para generar Pdf con codigos de barra
* ejemplo_bar.php un pequeño script para ver como funcionar la libreria
* Padron.csv es un archivo que contiene los votantes del distrito completo, con escuela y mesa
* Vehiculos padron de dominios
* Archivos en formato pdf que contiene la determinación del calculo
* Archivo en formato pdf que contiene la tabla para realizar el calculo del impuesto automotor

### Deberan realizar los siguientes ejercicios

## Leer el archivo Padron.csv y generar una salida en un archivo formato .pdf, el cual deberá tener el nombre Votantes+año+mes+dia+hora+min+seg.pdf
* La estructura de la salida debera ser la siguiente:
* Apellido | Nombre | DNI | Mesa | Escuela | Dirección de la escuela | Circuito Electoral
* La primer salida que se le recomienda para realizar es una salida sin importar el orden
* El segundo intento es realizar una salida pero que este ordenada en forma alfabtica, para poder buscar a los votantes de una mejor manera (opcional)

## En base al archivo Vehiculo.csv e instrucciones en formato .pdf
* Crear las tablas necesarias para el calculo 
# A modo de ejemplo les dejo una sugerencia
    *Vencimientos
    *Tabla_alicuotas
    *Periodos_dominios -> estas son sugerencias se puede debatir y generar algo mas solido 

* Para el calculo utilizaremos la valución del dominio, expuesto en el archivo, solamente dicho item utilizaremos para esta practica sin importar el tipo
* Crear la tabla de vencimientos para periodos fiscales del año en curso, teniendo en cuenta que hubieran 5 vencimientos
* Leer el archivo Vehiculos.csv como primer medida y en base a los archivos en formato .pdf comprender como es el calculo de la cuota, y agregar dicho importe como un dato mas al final del registro.
* Al mismo tiempo que se generan los peridos se deberá generar la deuda para los mismos
* Se pide realizar el modelo de recibo, en donde se pueda abonar el valor de la cuota, en donde el archivo tendra el nombre Recibo+Dominio+periodo+año+mes+dia+hora+min+seg.pdf , para todos los vehiculos.

Como siempre deberán generar el repositorio personal y compartir el mismo con el docente.
El siguiente es un trabajo de practica individual, el mismo no tiene front, por lo pronto se puede trabajar desde la consola para
realizar las pruebas o test necesarios.
Se recomienda agregar la libreria Fpdf , para una mejor implementación, como así también la lectura de sus tutoriales y documentación ya que la misma es bastante clara para este tipo de implementación.
