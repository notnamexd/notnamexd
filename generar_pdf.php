<?php
require('fpdf/fpdf.php');

if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET['patente']) && isset($_GET['cuotas'])) {
    // Obtener la patente y las cuotas seleccionadas
    $patente = $_GET['patente'];
    $cuotas_seleccionadas = explode(",", $_GET['cuotas']);

    // Ruta al archivo CSV de vehículos
    $ruta_vehiculos = 'Vehiculos.csv';

    // Ruta al archivo CSV de alícuotas
    $ruta_alicuotas = 'tabla_alicuotas.csv';

    // Ruta al archivo CSV de períodos y dominios
    $ruta_periodos = 'Periodo_Dominio.csv';

    // Inicializar las variables valor_vehiculo y valor_cuota en null
    $valor_vehiculo = null;
    $valor_cuota = null;
    $periodos = [];

    // Leer el archivo CSV de vehículos y buscar la coincidencia
    if (($handle = fopen($ruta_vehiculos, "r")) !== FALSE) {
        while (($row = fgetcsv($handle, 1000, "|")) !== FALSE) {
            // Compara la entrada con el valor en el índice 0
            if ($row[0] == $patente) {
                // Si hay una coincidencia, guarda el valor del índice 10 en valor_vehiculo
                $valor_vehiculo = $row[10];
                break; // Sal del bucle una vez que encuentres una coincidencia
            }
        }
        fclose($handle);
    }

    // Verificar si se encontró un valor para la patente
    if ($valor_vehiculo !== null) {
        // Cargar datos del archivo CSV de alícuotas
        $alicuotasData = [];
        if (($handle = fopen($ruta_alicuotas, "r")) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, "|")) !== FALSE) {
                $alicuotasData[] = $row;
            }
            fclose($handle);
        }

        // Encontrar el rango correspondiente y obtener el valor de la cuota
        foreach ($alicuotasData as $alicuota) {
            // Comprobar si el valor_vehiculo está dentro del rango
            if ($valor_vehiculo >= str_replace(".", "", $alicuota[1]) && $valor_vehiculo < str_replace(".", "", $alicuota[2])) {
                $valor_cuota = $alicuota[3];
                break;
            }
        }

        // Cargar datos del archivo CSV de períodos y dominios
        $periodoDominioData = [];
        if (($handle = fopen($ruta_periodos, "r")) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, "|")) !== FALSE) {
                $periodoDominioData[] = $row;
            }
            fclose($handle);
        }

        // Construir array de períodos
        foreach ($periodoDominioData as $data) {
            if ($data[1] == $patente) {
                for ($i = 2; $i < count($data); $i++) {
                    $periodos[] = $data[$i];
                }
                break;
            }
        }

        // Crear el objeto FPDF
        $pdf = new FPDF();
        $pdf->AddPage();

        // Agregar título
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(0,10,'Datos de vehículo y cuotas',0,1,'C');

        // Agregar datos de vehículo
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(0,10,"Patente del vehículo: $patente",0,1);
        $pdf->Cell(0,10,"Valor del vehículo: $valor_vehiculo",0,1);

        // Agregar datos de cuotas seleccionadas en forma de tabla
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(30,10,'Cuota',1,0,'C');
        $pdf->Cell(60,10,'Periodo',1,0,'C');
        $pdf->Cell(40,10,'Valor',1,0,'C');
        $pdf->Cell(40,10,'Vto',1,1,'C');

        $pdf->SetFont('Arial','',12);
        foreach ($cuotas_seleccionadas as $index => $cuota) {
            // Calcular el valor del vencimiento
            $vencimiento = $valor_cuota * 1.20;
            // Agregar fila a la tabla
            $pdf->Cell(30,10,"Cuota $cuota",1,0,'C');
            $pdf->Cell(60,10,isset($periodos[$index]) ? $periodos[$index] : "No disponible",1,0,'C');
            $pdf->Cell(40,10,"$$valor_cuota",1,0,'C');
            $pdf->Cell(40,10,"$$vencimiento",1,1,'C');
        }

        // Nombre del archivo PDF
        $pdf_name = 'vehiculo_cuotas.pdf';

        // Salida del PDF al navegador (descarga)
        $pdf->Output($pdf_name, 'D');
    } else {
        // Si no se encontró ninguna coincidencia, mostrar un mensaje de error
        echo "No se encontró ningún vehículo con la patente $patente";
    }
} else {
    // Si no se reciben los parámetros necesarios, redireccionar al formulario o mostrar un mensaje de error
    // Por ejemplo:
    // header("Location: formulario.php");
    echo "Error: No se recibieron los parámetros necesarios.";
}
?>
