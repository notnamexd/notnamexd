<?php
// Función para escribir datos en un archivo CSV con el separador |
function escribirCSV($filename, $data, $delimiter = '|') {
    if (($file = fopen($filename, 'w')) !== FALSE) {
        foreach ($data as $row) {
            fputcsv($file, $row, $delimiter);
        }
        fclose($file);
        echo "Archivo '$filename' generado correctamente.\n";
    } else {
        echo "Error al abrir el archivo '$filename' para escritura.\n";
    }
}

// Función para leer el archivo CSV y obtener los datos
function obtenerDatosCSV($filename, $delimiter = '|') {
    $data = [];
    if (($handle = fopen($filename, 'r')) !== FALSE) {
        // Saltar la primera línea (encabezado)
        fgetcsv($handle, 1000, $delimiter);
        // Leer las líneas restantes
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
            $data[] = $row;
        }
        fclose($handle);
    } else {
        echo "Error al abrir el archivo '$filename' para lectura.\n";
    }
    return $data;
}

// Función para generar períodos en base a la primera letra del dominio
function generarPeriodos($startDate, $meses, $letra) {
    $diasPorLetra = ord(strtoupper($letra)) - ord('A');
    $fechaInicio = strtotime("+$diasPorLetra days", strtotime($startDate));
    $periodos = [];

    for ($i = 0; $i < $meses; $i++) {
        $fechaFin = strtotime('+30 days', $fechaInicio);
        $periodos[] = date('d.m.Y', $fechaInicio) . ' - ' . date('d.m.Y', $fechaFin);
        $fechaInicio = strtotime('+1 month', $fechaInicio);
    }

    return $periodos;
}

// Datos para la tabla de alícuotas
$alicuotasData = [
    ["ID", "Mayor a","Menor o igual a","Cuota fija ($)", "Alicuota s/ excedente limite minimo %"],
    ["0", "0", "350.000", "0", "3,305"],
    ["1", "350.000", "420.000", "11.568", "4,446"],
    ["2", "420.000", "490.000", "14.680", "4,982"],
    ["3", "490.000", "560.000", "18.167", "5,298"],
    ["4", "560.000", "630.000", "21.876", "5,488"],
    ["5", "630.000", "700.000", "25.718", "5,584"],
    ["6", "700.000", "770.000", "29.627", "5,723"],
    ["7", "770.000", "840.000", "33.633", "5,805"],
    ["8", "840.000", "910.000", "37.697", "5,928"],
    ["9", "910.000", "980.000", "41.847", "6,006"],
    ["10", "980.000", "1.050.000", "46.051", "6,085"],
    ["11", "1.050.000", "1.190.000", "50.311", "6,178"],
    ["12", "1.190.000", "1.400.000", "58.960", "6,284"],
    ["13", "1.400.000", "8.000.000", "72.156", "6,323"],
    ["14", "8.000.000", "", "489.474", "6,370"],
];

// Generar y escribir los archivos CSV
$vehiculos = obtenerDatosCSV('Vehiculos.csv');
$startDate = '10.01.2024';

$periodoDominioData = [["ID", "Dominio", "Periodo 1", "Periodo 2", "Periodo 3", "Periodo 4", "Periodo 5"]];
$id = 1;

foreach ($vehiculos as $dominio) {
    $periodos = generarPeriodos($startDate, 5, $dominio[0]);
    $periodoDominioData[] = array_merge([$id++, $dominio[0]], $periodos);
}

escribirCSV('Periodo_Dominio.csv', $periodoDominioData);
escribirCSV('tabla_alicuotas.csv', $alicuotasData);

?>
