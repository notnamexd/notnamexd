<?php
require_once('fpdf/fpdf.php'); // Incluimos la librería FPDF

// Definimos la clase para generar el PDF de votantes
class PDF_Votantes extends FPDF {
    function Header() {
        $this->SetFont('Arial','B',15);
        $this->Cell(0,10,'Padron de Votantes',0,1,'C');
        $this->Ln(20);
    }

    function CreateTable($data) {
        $this->SetFont('Arial','',8); // Tamaño de fuente más pequeño
        foreach($data as $row) {
            $this->Cell(25,8,$row[2],1,0,'L'); // Apellido
            $this->Cell(40,8,$row[3],1,0,'L'); // Nombre
            $this->Cell(15,8,$row[0],1,0,'C'); // DNI
            $this->Cell(10,8,$row[8],1,0,'C'); // Mesa
            $this->Cell(58,8,$row[9],1,0,'L'); // Escuela
            $this->Cell(25,8,$row[10],1,0,'L'); // Dirección Escuela
            $this->Cell(10,8,$row[7],1,1,'C'); // Circuito Electoral
        }
    }
}

// Función para leer el archivo Padron.csv y devolver los datos
function leerPadron($archivo) {
    $votantes = [];
    if (($gestor = fopen($archivo, 'r')) !== FALSE) {
        while (($datos = fgetcsv($gestor, 1000, ';')) !== FALSE) {
            $votantes[] = $datos;
        }
        fclose($gestor);
    }
    return $votantes;
}

// Función para generar el PDF de votantes
function generarPDFVotantes($votantes) {
    $pdf = new PDF_Votantes();
    $pdf->AddPage();
    $pdf->CreateTable($votantes);
    $nombre_pdf = 'Votantes_' . date('YmdHis') . '.pdf';
    $pdf->Output($nombre_pdf, 'F');
    return $nombre_pdf;
}

// Leer el archivo Padron.csv (Asegúrate de que esté en UTF-8)
$votantes = leerPadron('Padron.csv');

// Generar el PDF de votantes
$nombre_pdf_votantes = generarPDFVotantes($votantes);

echo "PDF generado: $nombre_pdf_votantes";
?>
