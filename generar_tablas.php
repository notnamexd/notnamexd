<?php
require('fpdf/fpdf.php');

class PDF extends FPDF
{
    // Cabecera de página
    function Header()
    {
        $this->SetFont('Arial', 'B', 12);
        $this->Cell(0, 10, 'Vencimientos de Cuotas', 0, 1, 'C');
        $this->Ln(10);
    }

    // Pie de página
    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $this->Cell(0, 10, 'Página ' . $this->PageNo(), 0, 0, 'C');
    }
}

// Crear instancia de PDF
$pdf = new PDF();
$pdf->AddPage();

// Definir encabezados de tabla
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(30, 10, 'ID', 1, 0, 'C');
$pdf->Cell(60, 10, 'Vencimiento', 1, 1, 'C');

// Definir datos de la tabla
$id = 1;
$fecha = strtotime('15.02.2011');
$cuotas = 5;

$pdf->SetFont('Arial', '', 10);
for ($i = 0; $i < $cuotas; $i++) {
    $pdf->Cell(30, 10, $id++, 1, 0, 'C');
    $pdf->Cell(60, 10, date('d.m.Y', $fecha), 1, 1, 'C');
    $fecha = strtotime('+1 month', $fecha);
}

// Salida del PDF
$pdf->Output('F', 'Vencimientos.pdf');





// Datos para la tabla
$data = [
    ["0", "350.000", "0", "3,305"],
    ["350.000", "420.000", "11.568", "4,446"],
    ["420.000", "490.000", "14.680", "4,982"],
    ["490.000", "560.000", "18.167", "5,298"],
    ["560.000", "630.000", "21.876", "5,488"],
    ["630.000", "700.000", "25.718", "5,584"],
    ["700.000", "770.000", "29.627", "5,723"],
    ["770.000", "840.000", "33.633", "5,805"],
    ["840.000", "910.000", "37.697", "5,928"],
    ["910.000", "980.000", "41.847", "6,006"],
    ["980.000", "1.050.000", "46.051", "6,085"],
    ["1.050.000", "1.190.000", "50.311", "6,178"],
    ["1.190.000", "1.400.000", "58.960", "6,284"],
    ["1.400.000", "8.000.000", "72.156", "6,323"],
    ["8.000.000", "", "489.474", "6,370"],
];

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial', 'B', 10);

// Títulos de las columnas
$header = ["Base Imponible ($)", "Cuota fija ($)", "Alicuota s/ excedente limite minimo %"];
$subheader = ["Mayor a", "Menor igual a"];
$w = [30, 30, 60, 70]; // Ancho de las columnas

// Cabecera
$pdf->Cell($w[0] + $w[1], 10, $header[0], 1, 0, 'C');
$pdf->Cell($w[2], 10, $header[1], 1, 0, 'C');
$pdf->Cell($w[3], 10, $header[2], 1, 0, 'C');
$pdf->Ln();

// Subcabecera
$pdf->Cell($w[0], 10, $subheader[0], 1, 0, 'C');
$pdf->Cell($w[1], 10, $subheader[1], 1, 0, 'C');
$pdf->Cell($w[2], 10, '', 0, 0, 'C'); // Espacio en blanco bajo "Cuota fija"
$pdf->Cell($w[3], 10, '', 0, 0, 'C'); // Espacio en blanco bajo "Alicuota"
$pdf->Ln();

// Datos
$pdf->SetFont('Arial', '', 12);
foreach($data as $row) {
    $pdf->Cell($w[0], 10, $row[0], 1);
    $pdf->Cell($w[1], 10, $row[1], 1);
    $pdf->Cell($w[2], 10, $row[2], 1, 0, 'R');
    $pdf->Cell($w[3], 10, $row[3], 1, 0, 'R');
    $pdf->Ln();
}

$pdf->Output('F', 'tabla_alicuotas.pdf');
echo "PDF generado correctamente.";




// Función para leer el archivo CSV y obtener los datos
function obtenerDatosCSV($filename) {
    $data = [];
    if (($handle = fopen($filename, "r")) !== FALSE) {
        // Saltar la primera línea (encabezado)
        fgetcsv($handle, 1000, "|");
        // Leer las líneas restantes
        while (($row = fgetcsv($handle, 1000, "|")) !== FALSE) {
            $data[] = $row[0]; // Obtener el dato del índice 0
        }
        fclose($handle);
    }
    return $data;
}

// Datos del archivo CSV
$vehiculos = obtenerDatosCSV('Vehiculos.csv');

// Periodos
$periodos = [
    "1 periodo" => "15.01.2011 - 15.02.2011",
    "2 periodo" => "15.02.2011 - 15.03.2011",
    "3 periodo" => "15.03.2011 - 15.04.2011",
    "4 periodo" => "15.04.2011 - 15.05.2011",
    "5 periodo" => "15.05.2011 - 15.06.2011"
];

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial', 'B', 8);

// Títulos de las columnas
$pdf->Cell(20, 10, 'Dominio', 1);
$pdf->Cell(165, 10, 'Periodos', 1);
$pdf->Ln();

// Subcabecera para los periodos
$pdf->Cell(20, 10, '', 0); // Espacio en blanco bajo "Dominio"
foreach (array_keys($periodos) as $periodo) {
    $pdf->Cell(33, 10, $periodo, 1);
}
$pdf->Ln();

// Datos
$pdf->SetFont('Arial', '', 8);
foreach ($vehiculos as $dominio) {
    // Columna de dominio
    $pdf->Cell(20, 10, $dominio, 1);
    // Columnas de periodos
    foreach ($periodos as $dato) {
        $pdf->Cell(33, 10, $dato, 1);
    }
    $pdf->Ln();
}

$pdf->Output('F', 'Periodo_dominio.pdf');
echo "PDF generado correctamente.";





?>