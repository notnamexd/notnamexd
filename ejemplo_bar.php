<?php
require('code39.php');
$pdf = new PDF_Code39();
// no lo uso llamo a PDF_Code39() -->> $pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',10);
$pdf->SetXY(10,10);
$pdf->Cell(20,10,'Dominio',0,1);
$pdf->SetXY(30,10);
$pdf->Cell(20,10,'Titular',0,1);
$pdf->SetXY(50,10);
$pdf->Cell(20,10,'Vehiculo',0,1);
$pdf->SetXY(70,10);
$pdf->Cell(20,10,'Cuota',0,1);
$pdf->Code39(10,40, '12345678910',1,10);
$pdf->Output('','CuponDePago.pdf');
?>