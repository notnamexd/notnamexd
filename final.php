<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Obtener la patente del formulario
    $patente = $_POST['patente'];

    // Obtener las cuotas seleccionadas
    $cuotas_seleccionadas = isset($_POST['cuota']) ? $_POST['cuota'] : [];

    // Ruta al archivo CSV de vehículos
    $ruta_vehiculos = 'Vehiculos.csv';

    // Ruta al archivo CSV de alícuotas
    $ruta_alicuotas = 'tabla_alicuotas.csv';

    // Ruta al archivo CSV de períodos y dominios
    $ruta_periodos = 'Periodo_Dominio.csv';

    // Inicializar la variable valor_vehiculo en null
    $valor_vehiculo = null;

    // Inicializar la variable valor_cuota en null
    $valor_cuota = null;

    // Inicializar la variable periodo en null
    $periodo = null;

    // Leer el archivo CSV de vehículos y buscar la coincidencia
    if (($handle = fopen($ruta_vehiculos, "r")) !== FALSE) {
        while (($row = fgetcsv($handle, 1000, "|")) !== FALSE) {
            // Compara la entrada con el valor en el índice 0
            if ($row[0] == $patente) {
                // Si hay una coincidencia, guarda el valor del índice 10 en valor_vehiculo
                $valor_vehiculo = $row[10];
                break; // Sal del bucle una vez que encuentres una coincidencia
            }
        }
        fclose($handle);
    }

    // Verificar si se encontró un valor para la patente
    if ($valor_vehiculo !== null) {
        // Hacer algo con $valor_vehiculo, por ejemplo, imprimirlo
        echo "El valor del vehículo con patente $patente es: $valor_vehiculo";
        
        // Cargar datos del archivo CSV de alícuotas
        $alicuotasData = [];
        if (($handle = fopen($ruta_alicuotas, "r")) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, "|")) !== FALSE) {
                $alicuotasData[] = $row;
            }
            fclose($handle);
        }

        // Encontrar el rango correspondiente y obtener el valor de la cuota
        foreach ($alicuotasData as $alicuota) {
            // Comprobar si el valor_vehiculo está dentro del rango
            if ($valor_vehiculo >= str_replace(".", "", $alicuota[1]) && $valor_vehiculo < str_replace(".", "", $alicuota[2])) {
                $valor_cuota = $alicuota[3];
                break;
            }
        }

        if ($valor_cuota !== null) {
            echo "<br>El valor de la cuota correspondiente es:$ $valor_cuota";
        } else {
            echo "<br>No se encontró una cuota correspondiente para el valor del vehículo.";
        }

        // Cargar datos del archivo CSV de períodos y dominios
        $periodoDominioData = [];
        if (($handle = fopen($ruta_periodos, "r")) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, "|")) !== FALSE) {
                $periodoDominioData[] = $row;
            }
            fclose($handle);
        }

        // Crear la tabla
        echo "<table border='1'>";
        echo "<tr><th>Cuota</th><th>Periodo</th><th>Valor</th><th>Vto</th></tr>";
        
        // Buscar el dominio correspondiente al vehículo en el archivo de periodos y dominios
        foreach ($periodoDominioData as $data) {
            if ($data[1] == $patente) {
                // Asignar el periodo correspondiente a cada cuota seleccionada
                foreach ($cuotas_seleccionadas as $cuota) {
                    $indice_periodo = $cuota + 1; // Ajuste de índice para coincidir con los índices del archivo CSV
                    $periodo = isset($data[$indice_periodo]) ? $data[$indice_periodo] : "No disponible";
                    // Calcular el valor del vencimiento
                    $vencimiento = $valor_cuota * 1.20;
                    // Agregar fila a la tabla
                    echo "<tr><td>Cuota $cuota</td><td>$periodo</td><td>$$valor_cuota</td><td>$$vencimiento</td></tr>";
                }
                break;
            }
        }
        
        echo "</table>"; // Cerrar la tabla

        // Mostrar el enlace para generar el PDF
        echo "<br><a href='generar_pdf.php?patente=$patente&cuotas=" . implode(",", $cuotas_seleccionadas) . "'>Generar PDF</a>";
    } else {
        // Si no se encontró ninguna coincidencia, mostrar un mensaje de error
        echo "No se encontró ningún vehículo con la patente $patente";
    }
}
?>
