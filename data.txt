Base Imponible ($)     Cuota fija ($)        Alicuota s/ excedente límite mínimo (%)

Mayor a    Menor igual a
0          350.000         0                 3,305
350.000    420.000         11.568            4,446
420.000    490.000         14.680            4,982
490.000    560.000         18.167            5,298
560.000    630.000         21.876            5,488
630.000    700.000         25.718            5,584
700.000    770.000         29.627            5,723
770.000    840.000         33.633            5,805
840.000    910.000         37.697            5,928
910.000    980.000         41.847            6,006
980.000    1.050.000       46.051            6,085
1.050.000  1.190.000       50.311            6,178
1.190.000  1.400.000       58.960            6,284
1.400.000  8.000.000       72.156            6,323
8.000.000                  489.474            6,370





ahora necesito un codigo php que lea el dato introducido en el formulario y lo compare con el indice 0 de un archivo csv llamado Vehiculos.csv separado por | y empezando de la segunda linea, donde encuentre coincidencia tome el valor del indice 10 de esa misma linea e identifique donde se encuentra tomando en cuenta los datos de la columna Mayor a y Menor o igual a que estan dentro de otra columna llamada Base Imponible ($) de un archivo pdf llamado tabla_alicuotas.pdf, al coincidir tomar el valor de la columna Cuota fija ($) y crear un archivo pdf llamado Recibo+Dominio+periodo+año+mes+dia+hora+min+seg.pdf con titulo RECIBO que contenga una tabla con 2 columnas, la primera con encabezado Dominio y el dato introducido en el formulario, la segunda con encabezado Periodos que contenga 5 subcolumnas con encabezado periodo 1 , 2 , 3, 4, y 5 con los datos de las columnas periodo 1, 2, 3, 4 y 5 de un archivo pdf llamado Periodos_dominio.pdf y en segunda linea de periodo 1, 2, 3, 4 y 5 el valor que tomamos de Cuota fija ($)